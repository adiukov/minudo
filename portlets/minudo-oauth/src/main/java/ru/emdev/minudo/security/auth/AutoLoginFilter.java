package ru.emdev.minudo.security.auth;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.security.auth.AutoLoginException;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import ru.emdev.minudo.util.WebKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class AutoLoginFilter implements AutoLogin {

    private static final Log log = LogFactoryUtil.getLog(AutoLoginFilter.class);

    @Override
    public String[] handleException(HttpServletRequest request, HttpServletResponse response, Exception e) throws AutoLoginException {
        return null;
    }

    @Override
    public String[] login(HttpServletRequest request, HttpServletResponse response) throws AutoLoginException {
        log.debug("AutoLoginFilter fired...");
        String[] credentials = null;

        HttpSession session = request.getSession();

        try {
            long companyId = PortalUtil.getCompanyId(request);

            User user = null;
            long userId = 0;
            String oauthId = GetterUtil.getString((String)session.getAttribute(WebKeys.LIFERAY_SHARED + WebKeys.OAUTH_ID));
            if (Validator.isNotNull(oauthId)) {
                log.debug(String.format("Searching user with oauthId = [%s], companyId=[%d]", oauthId, companyId));
                try {
                    List<ExpandoValue> exValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
                            WebKeys.OAUTH_ID, String.valueOf(oauthId), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
                    if (exValues != null && exValues.size() > 0) {
                        ExpandoValue exValue = exValues.get(0);
                        userId = exValue.getClassPK();
                        user = UserLocalServiceUtil.getUser(userId);
                    }
                }
                catch (Exception nsue) {
                    log.debug(String.format("User with oauthId = [%s] was not found", oauthId));
                    return credentials;
                }

                if (user != null) {
                    log.debug(String.format("User with oauthId = [%s] was found. User id = [%d]", oauthId, user.getUserId()));
                    credentials = new String[3];

                    credentials[0] = String.valueOf(user.getUserId());
                    credentials[1] = user.getPassword();
                    credentials[2] = Boolean.FALSE.toString();
                } else {
                    log.debug(String.format("User with oauthId = [%s] was not found", oauthId));
                }
            } else {
                return credentials;
            }

        }
        catch (Exception e) {
            log.error(e, e);
        }

        return credentials;
    }
}
