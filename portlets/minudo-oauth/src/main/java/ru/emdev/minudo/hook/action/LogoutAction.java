package ru.emdev.minudo.hook.action;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import ru.emdev.minudo.util.OAuthUtil;
import ru.emdev.minudo.util.PortletKeys;
import ru.emdev.minudo.util.WebKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutAction extends Action {

    private static final Log log = LogFactoryUtil.getLog(LogoutAction.class);

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        long companyId = themeDisplay.getCompanyId();
        String redirect = themeDisplay.getURLSignOut();
        try {
            boolean oauthEnabled = PrefsPropsUtil.getBoolean(companyId, PortletKeys.OAUTH_ENABLED, Boolean.FALSE);
            if (oauthEnabled) {
                String logoutURL = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_LOGOUT_URL, StringPool.BLANK);
                String clientId = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_ID, StringPool.BLANK);
                String path = PrefsPropsUtil.getString(companyId, PropsKeys.DEFAULT_LOGOUT_PAGE_PATH, "https://dev.minudo.space");
                User user = themeDisplay.getUser();
                if (Validator.isNotNull(user)) {
                    String access_token = GetterUtil.getString(user.getExpandoBridge().getAttribute(WebKeys.OAUTH_TOKEN, false), StringPool.BLANK);
                    access_token = checkToken(companyId, access_token, user, request);
                    log.debug("logoutURL " + logoutURL);
                    log.debug("clientId " + clientId);
                    log.debug("path " + path);
                    log.debug("access_token " + access_token);
                    if (Validator.isNotNull(logoutURL) && Validator.isNotNull(clientId)
                            && Validator.isNotNull(path) && Validator.isNotNull(access_token)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(logoutURL);
                        sb.append("?client_id=" + clientId);
                        sb.append("&redirect_uri=" + path);
                        sb.append("&access_token=" + access_token);
                        redirect = sb.toString();
                    }
                }
            }
            log.debug("redirect " + redirect);
            if (!redirect.equals("/c/portal/logout")) {
                response.sendRedirect(redirect);
            }
        } catch (IOException | SystemException | PortalException e) {
            log.error(e);
        }
    }

    private String checkToken(long companyId, String token, User user, HttpServletRequest request) throws SystemException, PortalException {
        JSONObject userJSON = null;
        try {
            userJSON = OAuthUtil.checkToken(companyId, token);
        } catch (SystemException se) {
            String refreshToken = GetterUtil.getString(user.getExpandoBridge().getAttribute(WebKeys.REFRESH_TOKEN, false));
            JSONObject tokenObject = OAuthUtil.refreshToken(companyId, refreshToken);
            if (Validator.isNotNull(tokenObject)) {
                token = tokenObject.getString("access_token");
                refreshToken = tokenObject.getString("refresh_token");
                userJSON = OAuthUtil.getUserJson(companyId, tokenObject);
                OAuthUtil.setOauthCredentials(request.getSession(), companyId, userJSON, token, refreshToken);
            }
        }
        return token;
    }
}
