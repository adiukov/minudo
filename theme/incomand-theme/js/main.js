AUI().ready(
	'aui-carousel', 'liferay-hudcrumbs', 'liferay-navigation-interaction', 'event-outside', 'aui-sortable-layout', 'aui-toggler',
	function(A) {
		var navigation = A.one('#navigation');

		if (navigation) {
			navigation.plug(Liferay.NavigationInteraction);
		}

		var sonavigation = A.one('#so-navigation-bar');

		if (sonavigation) {
			sonavigation.plug(Liferay.NavigationInteraction);
		}

		var dockbar = A.one('.one-scheme .dockbar');

		if (dockbar) {
			A.all('#menuMeta .nav-item-dockbartoggle.metaNavi_link').each(function(btnDockbarToggle) {
				new A.Toggler(
					{
						content: dockbar,
						header: btnDockbarToggle,
						expanded: false
					}
				);
			});
		}

		var banner = A.one('.color-scheme-dashboard #banner');
		var btnNavigation = A.one('#navigation .nav-item-sitenavigationtoggle > a');
		var btnSoNavigation = A.one('#so-navigation-bar .so-navigation-toggle > a');

		if (banner) {
			var btnNavigationDockbar = A.one('#_145_navSiteNavigationNavbarBtn');

			if (!btnNavigationDockbar) {
				btnNavigationDockbar = btnNavigation;
			}
			else {
				btnNavigation.hide();
			}

			new A.Toggler(
				{
					content: banner,
					header: btnNavigationDockbar,
					expanded: false
				}
			);
		}
		else {
			var navigation = A.one(Liferay.Data.NAV_SELECTOR);

			if (btnNavigation != null){
				btnNavigation.on('click', function(event) {
					btnNavigation.toggleClass('open');
					navigation.toggleClass('open');
				});
			}
			if (btnSoNavigation != null){
				btnSoNavigation.on('click', function(event) {
					btnSoNavigation.toggleClass('open');
					sonavigation.toggleClass('open');
				});
			}
		}
	}
);

Liferay.on(
	'allPortletsReady',
	function() {
		AUI().use("aui-node", function (A) {
			A.all("[id$=_3d-tag-canvas]").each(function () {
				var canvas = this;
				var canvasId = canvas.get("id");
				var tagList = canvas.next();
				var tagListId = tagList.get("id");
				try {
					var weightSizeMin = canvas.getData("min");
					if (weightSizeMin === undefined) {
						weightSizeMin = 20;
					}
					else {
						weightSizeMin = +weightSizeMin;
					}
					var weightSizeMax = canvas.getData("max");
					if (weightSizeMax === undefined) {
						weightSizeMax = 60;
					}
					else {
						weightSizeMax = +weightSizeMax;
					}
					var minBrightness = canvas.getData("fade");
					if (minBrightness === undefined) {
						minBrightness = .05;
					}
					else {
						minBrightness = +minBrightness;
					}
					var maxSpeed = canvas.getData("speed");
					if (maxSpeed === undefined) {
						maxSpeed = .05;
					}
					else {
						maxSpeed = +maxSpeed;
					}
					TagCanvas.Start(canvasId, tagListId, {
						clickToFront: 200,
						initial: [0.180, -0.120],
						maxSpeed: maxSpeed,
						textColour : null,
						reverse: true,
						fadeIn: 100,
						freezeActive: true,
						freezeDecel: true,
						frontSelect: true,
						minBrightness: minBrightness,
						outlineMethod: "none",
						weight: true,
						weightFrom: "data-freq",
						weightSizeMin: weightSizeMin,
						weightSizeMax: weightSizeMax
					});
					canvas.show();
				}
				catch (e) {
					tagList.show();
				}
			});
		});
	}
);
