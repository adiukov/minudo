package ru.emdev.minudo.util;

public class JSONKeys {

    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String MIDDLE_NAME = "middlename";
    public static final String EMAIL = "email";
    public static final String ROLES = "roles";
    public static final String GROUPS = "groups";
    public static final String SCOPES = "scopes";
    public static final String PRINCIPAL = "principal";
    public static final String ATTRIBUTES = "attributes";
}
