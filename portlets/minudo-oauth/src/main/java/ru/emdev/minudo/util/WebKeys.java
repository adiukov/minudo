package ru.emdev.minudo.util;

public class WebKeys implements com.liferay.portal.kernel.util.WebKeys {

    public static final String OAUTH_ID = "oauthId";
    public static final String OAUTH_TOKEN = "oauth_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String LIFERAY_SHARED = "LIFERAY_SHARED_";
    public static final String DEPT_NAME = "deptName";
}
