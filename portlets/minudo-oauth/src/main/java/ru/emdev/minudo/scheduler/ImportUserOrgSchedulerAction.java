package ru.emdev.minudo.scheduler;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import ru.emdev.minudo.util.ImportUtil;

public class ImportUserOrgSchedulerAction implements MessageListener {

    private static final Log log = LogFactoryUtil.getLog(ImportUserOrgSchedulerAction.class);

    @Override
    public void receive(Message message) throws MessageListenerException {
        ImportUtil.startImport();
    }
}
