<%@ include file="/html/init.jsp" %>

<%
  PortletPreferences preferences = renderRequest.getPreferences();
  String portletResource = ParamUtil.getString(request, "portletResource");
  String omURL = "";
  if (Validator.isNotNull(portletResource)) {
    preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
    omURL = preferences.getValue("omURL", "");
  }
%>

<liferay-portlet:actionURL portletConfiguration="true" var="saveConfigurationURL"/>
<aui:form action="<%=saveConfigurationURL %>" method="post" name="fm">
	<input name="<portlet:namespace /><%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />
<aui:input name="preferences--omURL--" type="text" label="om-url" value="<%= omURL %>" />

  <aui:button name="" type="submit" label="save"/>
</aui:form>