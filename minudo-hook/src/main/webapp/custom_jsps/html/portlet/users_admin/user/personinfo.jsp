<%@ include file="/html/portlet/users_admin/init.jsp" %>

<%
    String preferenceVueJS = "<portlet-preferences> \n" +
            "        <preference>\n" +
            "               <name>omURL</name>\n" +
            "               <value>https://lr.dev.minudo.space/cust/#/personal</value>\n" +
            "         </preference>\n" +
            "    </portlet-preferences>\n";
%>
<liferay-portlet:runtime portletName="vuejsportlet_WAR_vuejsportlet" defaultPreferences="<%=preferenceVueJS%>"/>