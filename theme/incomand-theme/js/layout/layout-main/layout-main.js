YUI.add('layout-main', function(Y) {
	var upElem = document.getElementById('upbutton');
	var upElem_show_scroll = 183;
    var wrapper = document.getElementById('wrapper');
    var fixed_nav_scroll = 115;
    
	
	upElem.onclick = function() {
		smoothJumpUp();
		return false;
	}
	window.onscroll = function() {
		var pageY = window.pageYOffset || document.documentElement.scrollTop;
		upElem.className = (pageY > upElem_show_scroll ? 'visible' : 'hide');
        wrapper.className = (pageY > fixed_nav_scroll ? 'fixed-nav' : ' ');
	}
	
	smoothJumpUp = function() {
		if (document.body.scrollTop > 0	|| document.documentElement.scrollTop > 0) {
			window.scrollBy(0, -20);
			setTimeout(smoothJumpUp, 2);
		}
	}
    
    var parent = document.getElementById('column-1');
    var contentBlock = document.getElementById('layout-column_column-1');
    
    if (parent && contentBlock) {
        Y.Node.create('<div class=\'icon-toggler icon-toggler1\'> </div>').appendTo(parent);

        YUI().use(
              'aui-toggler',
              function(Y) {
                new Y.Toggler(
                  {
                    container: '#column-1',
                    content: '#layout-column_column-1',
                    expanded: true,
                    header: '.icon-toggler1'
                  }
                );
              }
            );
    }


}, '1.0', {
	requires : [],
	async : false
});