package ru.emdev.minudo.events;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import ru.emdev.minudo.util.WebKeys;

public class StartUpAction extends SimpleAction {

    private static final Log log = LogFactoryUtil.getLog(StartUpAction.class);

    @Override
    public void run(String[] strings) throws ActionException {
        try {
            for (String id : strings) {
                long companyId = Long.parseLong(id);
                createCustomAttributes(companyId);
            }
        } catch (Exception e) {
            log.error("Initialization failed", e);
            throw new ActionException(e);
        }
    }

    private void createCustomAttributes(long companyId) throws PortalException, SystemException {
        ExpandoTable userExandoTable = null;
        try {
            userExandoTable = ExpandoTableLocalServiceUtil.getTable(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME);
        } catch (Exception ex) {}

        if (userExandoTable == null) {
            userExandoTable = ExpandoTableLocalServiceUtil.addTable(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME);
        }
        ExpandoColumn column = null;
        try {
            column = ExpandoColumnLocalServiceUtil.getColumn(userExandoTable.getTableId(), WebKeys.OAUTH_ID);
        } catch (Exception ex) {}
        if (column == null) {
            ExpandoColumnLocalServiceUtil.addColumn(userExandoTable.getTableId(), WebKeys.OAUTH_ID, ExpandoColumnConstants.STRING);
        }
    }
}
