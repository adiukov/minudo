package ru.emdev.minudo.hook.action;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import org.apache.http.HttpStatus;
import ru.emdev.minudo.util.OAuthUtil;
import ru.emdev.minudo.util.WebKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CheckToken extends BaseStrutsAction {

    private static final Log log = LogFactoryUtil.getLog(CheckToken.class);

    private static final String ERROR = "error";

    @Override
    public String execute(
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        JSONObject result = JSONFactoryUtil.createJSONObject();
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (themeDisplay.isSignedIn()) {
            String token = ParamUtil.getString(request, "token", StringPool.BLANK);
            if (!token.isEmpty()) {
                long companyId = themeDisplay.getCompanyId();
                User user = themeDisplay.getUser();
                JSONObject userJSON = null;
                try {
                    userJSON = OAuthUtil.checkToken(companyId, token);
                } catch (SystemException se) {
                    try {
                        String refreshToken = GetterUtil.getString(user.getExpandoBridge().getAttribute(WebKeys.REFRESH_TOKEN, false));
                        JSONObject tokenObject = OAuthUtil.refreshToken(companyId, refreshToken);
                        if (Validator.isNotNull(tokenObject)) {
                            token = tokenObject.getString("access_token");
                            refreshToken = tokenObject.getString("refresh_token");
                            userJSON = OAuthUtil.getUserJson(companyId, tokenObject);
                            OAuthUtil.setOauthCredentials(request.getSession(), companyId, userJSON, token, refreshToken);
                        }
                    } catch (SystemException e) {
                        result.put(ERROR, e.getMessage());
                    }
                }
                if (Validator.isNotNull(userJSON)) {
                    result.put("token", token);
                } else {
                    result.put("logout", themeDisplay.getURLSignOut());
                }
            } else {
                result.put(ERROR, "token in request is empty");
            }
        } else {
            result.put(ERROR, "user is not login");
            result.put("login", themeDisplay.getURLSignIn());
        }
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpStatus.SC_OK);
        try {
            PrintWriter writer = response.getWriter();
            writer.print(result.toString());
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            writeErrorDebugLevel(ex, log, "Unable to write result in http response");
        }
        return null;
    }

    private void writeErrorDebugLevel(Exception ex, Log log, String... args) {
        String message = StringPool.BLANK;
        for (String arg : args) {
            message += arg + StringPool.BLANK;
        }
        message += "for more info turn on debug log level!!!";
        log.error(message);
        log.debug(ex);
    }
}
