package ru.emdev.minudo.portlet;

import com.liferay.util.bridges.mvc.MVCPortlet;
import ru.emdev.minudo.util.ImportUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

public class ImportPortlet extends MVCPortlet {

    public void importAction(ActionRequest actionRequest, ActionResponse actionResponse) {
        ImportUtil.startImport();
    }
}
