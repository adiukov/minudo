<%@ include file="/html/portlet/portal_settings/init.jsp" %>

<%
    boolean connectAuthEnabled = PrefsPropsUtil.getBoolean(company.getCompanyId(), "OAUTH_ENABLED", false);
    String oauthScope = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_SCOPE", "worker");
    String connectAppId = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_CLIENT_ID", "lray");
    String connectAppSecret = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_CLIENT_SECRET", "505341f4-7f1b-4041-b95c37ee0d54502d");
    String connectGraphURL = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_URL", "https://dev.minudo.space/saas/oauth2/authorize");
    /*String connectRedirectURL = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_REDIRECT_URL", StringPool.BLANK);*/
    String authTokenURL = PrefsPropsUtil.getString(company.getCompanyId(),"OAUTH_TOKEN_URL", "https://dev.minudo.space/saas/oauth2/token");
    String authLogoutURL = PrefsPropsUtil.getString(company.getCompanyId(),"OAUTH_LOGOUT_URL", "https://dev.minudo.space/saas/oauth2/logout");
    String authTokenCheckURL = PrefsPropsUtil.getString(company.getCompanyId(),"OAUTH_TOKEN_CHECK_URL", "https://dev.minudo.space/saas/oauth2/tokeninfo");
    String resourceServerKey = PrefsPropsUtil.getString(company.getCompanyId(),"RESOURCE_SERVER_KEY", "9b3f84f2-2717-40fc-b908-c702cf035b56");
    String resourceServerPassword = PrefsPropsUtil.getString(company.getCompanyId(),"RESOURCE_SERVER_PASSWORD", "8da884bd-1229-4927-97d5-9fc0b83b89b6");
    String connectAppIdImport = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_CLIENT_ID_IMPORT", "lraysync");
    String connectAppSecretImport = PrefsPropsUtil.getString(company.getCompanyId(), "OAUTH_CLIENT_SECRET_IMPORT", "353df433-1aa1-4382-8792-7ddf81ec393e");
    String apiURL = PrefsPropsUtil.getString(company.getCompanyId(), "API_URL", "https://dev.minudo.space/api");
    boolean deleteImportEnable = PrefsPropsUtil.getBoolean(company.getCompanyId(), "IMPORT_DELETE_ENABLED", true);
%>

<aui:fieldset>
    <aui:input label="enabled" name='<%= "settings--" + "OAUTH_ENABLED" + "--" %>' type="checkbox" value="<%= connectAuthEnabled %>" />

    <aui:input cssClass="lfr-input-text-container" label="oauth-scope" name='<%= "settings--" + "OAUTH_SCOPE" + "--" %>' type="text" value="<%= oauthScope %>" />

    <aui:input cssClass="lfr-input-text-container" label="application-id" name='<%= "settings--" + "OAUTH_CLIENT_ID" + "--" %>' type="text" value="<%= connectAppId %>" />

    <aui:input cssClass="lfr-input-text-container" label="application-secret" name='<%= "settings--" + "OAUTH_CLIENT_SECRET" + "--" %>' type="text" value="<%= connectAppSecret %>" />

    <aui:input cssClass="lfr-input-text-container" label="oauth-authentication-url" name='<%= "settings--" + "OAUTH_URL" + "--" %>' type="text" value="<%= connectGraphURL %>" />

    <%--<aui:input cssClass="lfr-input-text-container" label="redirect-url" helpMessage="redirect-url-help-message" name='<%= "settings--" + "OAUTH_REDIRECT_URL" + "--" %>' type="text" value="<%= connectRedirectURL %>" />--%>

    <aui:input cssClass="lfr-input-text-container" label="oauth-token-url" name='<%= "settings--" + "OAUTH_TOKEN_URL" + "--" %>' type="text" value="<%= authTokenURL %>" />

    <aui:input cssClass="lfr-input-text-container" label="oauth-token-check-url" name='<%= "settings--" + "OAUTH_TOKEN_CHECK_URL" + "--" %>' type="text" value="<%= authTokenCheckURL %>" />

    <aui:input cssClass="lfr-input-text-container" label="resource-server-key" name='<%= "settings--" + "RESOURCE_SERVER_KEY" + "--" %>' type="text" value="<%= resourceServerKey %>" />

    <aui:input cssClass="lfr-input-text-container" label="resource-server-password" name='<%= "settings--" + "RESOURCE_SERVER_PASSWORD" + "--" %>' type="text" value="<%= resourceServerPassword %>" />

    <aui:input cssClass="lfr-input-text-container" label="oauth-logout-url" name='<%= "settings--" + "OAUTH_LOGOUT_URL" + "--" %>' type="text" value="<%= authLogoutURL %>" />

    <hr/>

    <aui:input cssClass="lfr-input-text-container" label="application-id-import" name='<%= "settings--" + "OAUTH_CLIENT_ID_IMPORT" + "--" %>' type="text" value="<%= connectAppIdImport %>" />

    <aui:input cssClass="lfr-input-text-container" label="application-secret-import" name='<%= "settings--" + "OAUTH_CLIENT_SECRET_IMPORT" + "--" %>' type="text" value="<%= connectAppSecretImport %>" />

    <aui:input cssClass="lfr-input-text-container" label="api-url" name='<%= "settings--" + "API_URL" + "--" %>' type="text" value="<%= apiURL %>" />

    <aui:input label="import-delete-enabled" name='<%= "settings--" + "IMPORT_DELETE_ENABLED" + "--" %>' type="checkbox" value="<%= deleteImportEnable %>" />

</aui:fieldset>