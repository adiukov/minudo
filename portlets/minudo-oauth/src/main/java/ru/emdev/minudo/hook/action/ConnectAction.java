package ru.emdev.minudo.hook.action;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import ru.emdev.minudo.util.*;
import ru.emdev.minudo.util.WebKeys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ConnectAction extends BaseStrutsAction {

    private static final String SUCCESS_REDIRECT_URL = "/portlet/login/oauth_redirect.jsp";

    private static final Log log = LogFactoryUtil.getLog(ConnectAction.class);

    @Override
    public String execute(
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        Thread currentThread = Thread.currentThread();

        ClassLoader contextClassLoader =
                currentThread.getContextClassLoader();

        currentThread.setContextClassLoader(
                PortalClassLoaderUtil.getClassLoader());

        String state = ParamUtil.getString(request, "state");

        String redirect = state.contains("?redirect=") ? state.split("=")[1] : state;

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        try {
            long companyId = themeDisplay.getCompanyId();
            String scope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE);
            log.debug("Logging in through oauth...");

            String code = ParamUtil.getString(request, "code");

            JSONObject accessToken = OAuthUtil.getAccessToken(companyId, themeDisplay.getURLPortal() + "/c/login/oauth", code, scope);
            log.debug("oauth access token = " + accessToken.getString("access_token"));

            JSONObject userJson = OAuthUtil.getUserJson(companyId, accessToken);
            log.debug("oauth user  = " + userJson.toString());

            HttpSession session = request.getSession();

            OAuthUtil.setOauthCredentials(session, themeDisplay.getCompanyId(), userJson, accessToken.getString("access_token"), accessToken.getString("refresh_token"));
        } catch (Exception e) {
            SystemException se = new SystemException("error-oauth", e);
            SessionErrors.add(request, "error-oauth", se);
            PortalUtil.sendError(se, request, response);
            return null;
        } finally {
            currentThread.setContextClassLoader(contextClassLoader);
        }
        log.debug("redirect: " + redirect);
        request.setAttribute(WebKeys.REDIRECT, redirect);

		return SUCCESS_REDIRECT_URL;
    }
}
