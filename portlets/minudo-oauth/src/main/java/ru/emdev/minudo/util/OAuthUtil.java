package ru.emdev.minudo.util;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.*;
import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OAuthUtil {

    private static final Log log = LogFactoryUtil.getLog(OAuthUtil.class);

    public static JSONObject checkToken(long companyId, String token) throws SystemException {
        String scope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE, StringPool.BLANK);
        return getUserJson(companyId, token, scope);
    }

    public static JSONObject getAccessToken(long companyId, String redirect, String code, String scope)
            throws SystemException {

        String url = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_TOKEN_URL);

        log.debug("url " + url);

        Http.Options options = new Http.Options();
        options.setLocation(url);
        options.setPost(true);

        try {
            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
            urlParameters.add(new BasicNameValuePair("client_id", PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_ID)));
            urlParameters.add(new BasicNameValuePair("client_secret", PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_SECRET)));
            urlParameters.add(new BasicNameValuePair("code", code));
            urlParameters.add(new BasicNameValuePair("redirect_uri", redirect));
            String content = RequestUtil.sendPost(url, urlParameters, null);
            log.debug("oauth content = " + content);
            JSONObject json = getJsonObject(content, scope);
            if (json != null) return json;
        } catch (Exception e) {
            throw new SystemException(
                    "Unable to retrieve oauth access token", e);
        }

        return null;
    }

    public static JSONObject refreshToken(long companyId, String refreshToken) throws SystemException {
        String url = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_TOKEN_URL);
        String scope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE);
        log.debug("url " + url);

        Http.Options options = new Http.Options();
        options.setLocation(url);
        options.setPost(true);

        try {
            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
            urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));
            urlParameters.add(new BasicNameValuePair("client_id", PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_ID)));
            urlParameters.add(new BasicNameValuePair("client_secret", PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_SECRET)));
            String content = RequestUtil.sendPost(url, urlParameters, null);
            log.debug("oauth content = " + content);
            JSONObject json = getJsonObject(content, scope);
            if (json != null) return json;
        } catch (Exception e) {
            throw new SystemException(
                    "Unable to retrieve oauth access token", e);
        }

        return null;
    }

    public static JSONObject getUserJson(long companyId, JSONObject token)
            throws SystemException {
        String scope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE);
        if ((token == null) ||
                (token.getJSONObject("error") != null)) {
            log.debug("Error in access token");
            return null;
        }
        if (token == null || Validator.isNull(token.getString("scope")) || Validator.isNull(token.getString("access_token"))) {
            log.debug("Failed to retrieve required info from OAuth access token");
            return null;
        }
        return getUserJson(companyId, token.getString("access_token"), scope);
    }

    public static JSONObject getUserJson(long companyId, String token, String scope) throws SystemException {
        String url = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_TOKEN_CHECK_URL);

        String base64 = PrefsPropsUtil.getString(companyId, PortletKeys.RESOURCE_SERVER_KEY) + ":" + PrefsPropsUtil.getString(companyId, PortletKeys.RESOURCE_SERVER_PASSWORD);
        base64 = Base64.encode(base64.getBytes());
        log.debug("base64 for encode " + base64);
        try {
            log.debug("base64 " + base64);
            Header basic = new BasicHeader("Authorization", "Basic " + base64);
            Header[] headers = new Header[] {basic};
            url = HttpUtil.addParameter(url, "access_token", token);
            String content = RequestUtil.sendGet(url, headers);
            log.debug("Oauth user content = " + content);
            JSONObject json = getJsonObject(content, scope);
            String email = StringPool.BLANK;
            if (json.has("principal")) {
                JSONObject principal = json.getJSONObject("principal");
                JSONObject attributes = principal.getJSONObject("attributes");
                email = ImportUtil.getJsonKey(attributes, "email");
            } else {
                email = ImportUtil.getJsonKey(json, "email");
            }
            if (Validator.isNull(email) || email.isEmpty()
                || !Validator.isEmailAddress(email)) {
                throw new SystemException("email is empty or invalid for " + json.toString());
            }
            if (json != null) return json;
        } catch (Exception e) {
            throw new SystemException(
                    "Unable to retrieve oauth user", e);
        }
        return null;
    }

    private static JSONObject getJsonObject(String content, String scope) throws JSONException, SystemException {
        if (Validator.isNotNull(content)) {
            JSONObject json = JSONFactoryUtil.createJSONObject(content);
            if (json.has("error")) {
                throw new SystemException(json.getString("error_description"));
            } else if (json.has("scopes")) {
                JSONArray scopes = json.getJSONArray("scopes");
                for (int i = 0; i < scopes.length(); i++) {
                    String curScope = scopes.getString(i);
                    if (curScope.equals(scope)) {
                        return json;
                    }
                }
                throw new SystemException("need scope " + scope);
            } else {
                return json;
            }
        }
        return null;
    }

    public static void setOauthCredentials(HttpSession session, long companyId, JSONObject userJson, String accessToken, String refreshToken) throws PortalException, SystemException {
        log.debug(userJson.toString());
        JSONObject attributes = userJson.getJSONObject("principal").getJSONObject("attributes");
        String email = attributes.getString("email");
        String oauthId = attributes.getString("userId");
        log.debug("email = " + email);
        log.debug("userId = " + oauthId);
        if (attributes == null || Validator.isNull(email) || Validator.isNull(oauthId) ) {
            throw new NullPointerException("Failed to retrieve required info from OAuth user token");
        }
        User user = null;
        if (Validator.isNotNull(oauthId) && Validator.isNotNull(email)) {
            long userId = 0;
            List<ExpandoValue> exValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId, User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
                    WebKeys.OAUTH_ID, oauthId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            if (exValues != null && exValues.size() > 0) {
                ExpandoValue exValue = exValues.get(0);
                userId = exValue.getClassPK();
                user = UserLocalServiceUtil.getUser(userId);
            }
            if (user == null) {
                user = addUser(session, companyId, userJson);
            } else {
                user = updateUser(user, userJson);
            }
            ExpandoBridge expandoBridge = user.getExpandoBridge();
            if (!expandoBridge.hasAttribute(WebKeys.OAUTH_ID)) {
                expandoBridge.addAttribute(WebKeys.OAUTH_ID, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
            }
            expandoBridge.setAttribute(WebKeys.OAUTH_ID, oauthId, false);
            if (!expandoBridge.hasAttribute(WebKeys.OAUTH_TOKEN)) {
                expandoBridge.addAttribute(WebKeys.OAUTH_TOKEN, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
            }
            expandoBridge.setAttribute(WebKeys.OAUTH_TOKEN, accessToken, false);
            if (!expandoBridge.hasAttribute(WebKeys.REFRESH_TOKEN)) {
                expandoBridge.addAttribute(WebKeys.REFRESH_TOKEN, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
            }
            expandoBridge.setAttribute(WebKeys.REFRESH_TOKEN, refreshToken, false);
            session.setAttribute(WebKeys.LIFERAY_SHARED + WebKeys.OAUTH_ID, oauthId);
        }
    }

    private static User addUser(HttpSession session, long companyId, JSONObject userJson) throws SystemException, PortalException {
        User user = null;
        //try {
            JSONObject principal = userJson.getJSONObject(JSONKeys.PRINCIPAL);
            JSONArray roles = principal.getJSONArray(JSONKeys.ROLES);
            JSONArray groups = principal.getJSONArray(JSONKeys.GROUPS);
            JSONObject attributes = principal.getJSONObject(JSONKeys.ATTRIBUTES);
            log.debug("attributes " + attributes.toString());
            long companyAdminId = UserLocalServiceUtil.getDefaultUserId(companyId);
            int birthdayMonth = Calendar.JANUARY;
            int birthdayDay = 1;
            int birthdayYear = 1970;
            long[] roleIds = getRoles(companyId, roles);
            long[] regularRoles = filterRoles(roleIds, RoleConstants.TYPE_REGULAR);
            long[] siteRoles = filterRoles(roleIds, RoleConstants.TYPE_SITE);
            long[] groupsIds = getGroups(companyId, groups);
            String email = attributes.getString(JSONKeys.EMAIL);
            String firstName = attributes.getString(JSONKeys.FIRST_NAME);
            String lastName = attributes.getString(JSONKeys.LAST_NAME);
            String middleName = attributes.getString(JSONKeys.MIDDLE_NAME);
            user = UserLocalServiceUtil.addUser(companyAdminId, companyId, true,
                    StringPool.BLANK, StringPool.BLANK, true, StringPool.BLANK,
                    email, 0l, StringPool.BLANK, null,
                    firstName, middleName,
                    lastName, 0, 0, true,
                    birthdayMonth, birthdayDay, birthdayYear, StringPool.BLANK,
                    groupsIds, null, regularRoles, null, false, new ServiceContext());
        setGroupRoles(user.getUserId(), groupsIds, siteRoles);
        /*} catch (SystemException | PortalException e) {
            log.error(e);
        }*/
        return user;
    }

    private static User updateUser(User user, JSONObject userJson) throws SystemException, PortalException {
        JSONObject principal = userJson.getJSONObject(JSONKeys.PRINCIPAL);
        JSONArray roles = principal.getJSONArray(JSONKeys.ROLES);
        JSONArray groups = principal.getJSONArray(JSONKeys.GROUPS);
        JSONObject attributes = principal.getJSONObject(JSONKeys.ATTRIBUTES);
        String email = attributes.getString(JSONKeys.EMAIL);
        String firstName = attributes.getString(JSONKeys.FIRST_NAME);
        String lastName = attributes.getString(JSONKeys.LAST_NAME);
        String middleName = attributes.getString(JSONKeys.MIDDLE_NAME);
        long[] roleIds = getRoles(user.getCompanyId(), roles);
        long[] regularRoles = filterRoles(roleIds, RoleConstants.TYPE_REGULAR);
        long[] siteRoles = filterRoles(roleIds, RoleConstants.TYPE_SITE);
        long[] groupsIds = getGroups(user.getCompanyId(), groups);
        user.setEmailAddress(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        //try {
            user = UserLocalServiceUtil.updateUser(user);
            RoleLocalServiceUtil.setUserRoles(user.getUserId(), regularRoles);
            GroupLocalServiceUtil.addUserGroups(user.getUserId(), groupsIds);
            setGroupRoles(user.getUserId(), groupsIds, siteRoles);
        /*} catch (PortalException | SystemException e) {
            log.error(e);
        }*/
        return user;
    }

    private static long[] getRoles(long companyId, JSONArray roles) {
        long[] roleIds = null;
        List<Long> roleIdList = new ArrayList<Long>();
        for (int i = 0; i < roles.length(); i++) {
            String roleName = roles.getString(i);
            Role role = null;
            try {
                role = RoleLocalServiceUtil.fetchRole(companyId, roleName);
            } catch (SystemException e) {
                log.error(e.getMessage());
            }
            if (Validator.isNotNull(role)) {
                roleIdList.add(role.getRoleId());
            }
        }
        roleIds = new long[roleIdList.size()];
        for (int i = 0; i < roleIdList.size(); i++) {
            roleIds[i] = roleIdList.get(i);
        }
        return roleIds;
    }

    private static long[] getGroups(long companyId, JSONArray groups) {
        long[] groupIds = null;
        List<Long> groupIdList = new ArrayList<Long>();
        for (int i = 0; i < groups.length(); i++) {
            String groupName = groups.getString(i);
            Group group = null;
            try {
                group = GroupLocalServiceUtil.fetchFriendlyURLGroup(companyId, StringPool.SLASH + groupName);
            } catch (SystemException e) {
                log.error(e.getMessage());
            }
            if (Validator.isNotNull(group)) {
                groupIdList.add(group.getGroupId());
            }
        }
        groupIds = new long[groupIdList.size()];
        for (int i = 0; i < groupIdList.size(); i++) {
            groupIds[i] = groupIdList.get(i);
        }
        return groupIds;
    }

    private static void setGroupRoles(long userId, long[] groupIds, long[] roleIds) throws SystemException {
        UserGroupRoleLocalServiceUtil.deleteUserGroupRolesByUserId(userId);
        if (roleIds.length > 0) {
            for (long groupId: groupIds) {
                UserGroupRoleLocalServiceUtil.addUserGroupRoles(userId, groupId, roleIds);
            }
        }
    }

    private static long[] filterRoles(long[] roleIds, int roleType) {
        long result[] = new long[0];
        if (ArrayUtil.isNotEmpty(roleIds)) {
            List<Long> roleIdList = new ArrayList<Long>();
            for (long id: roleIds) {
                Role role = null;
                try {
                    role = RoleLocalServiceUtil.fetchRole(id);
                } catch (SystemException e) {
                    log.error(e.getMessage());
                }
                if (Validator.isNotNull(role)) {
                    if (role.getType() == roleType) {
                        roleIdList.add(role.getRoleId());
                    }
                }
            }
            result = ArrayUtil.toLongArray(roleIdList);
        }
        return result;
    }
}
