package ru.emdev.minudo.util;

import com.liferay.portal.DuplicateOrganizationException;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.UserEmailAddressException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.model.*;
import com.liferay.portal.service.*;
import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportUtil {

    private static final String STATUS_A = "A";
    private static final String STATUS_D = "D";
    private static final String ORGS_URI = "/sync/orgs";
    private static final String DEPTS_URI = "/sync/depts";
    private static final String USERS_URI = "/sync/users";

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    private static final Log log = LogFactoryUtil.getLog(ImportUtil.class);

    public static void startImport() {
        startImportOrganization();
        startImportUsers();
    }

    public static void startImportOrganization() {
        List<Company> companies = null;
        try {
            companies = CompanyLocalServiceUtil.getCompanies();
        } catch (Exception ex) {
            log.error(ex);
        }
        if (companies != null && !companies.isEmpty()) {
            for (Company company: companies) {
                try {
                    if (GetterUtil.getBoolean(PrefsPropsUtil.getBoolean(company.getCompanyId(),
                            PortletKeys.OAUTH_ENABLED, false))) {
                        getOrganizations(company.getCompanyId());
                    }
                } catch (SystemException se) {
                    log.error("Can't import organizations for " + company.getCompanyId(), se);
                }
            }
        }
    }

    private static void getOrganizations(long companyId) throws SystemException {
        String token = getAppToken(companyId);
        String apiURL = PrefsPropsUtil.getString(companyId, PortletKeys.API_URL, StringPool.BLANK);
        String orgsURL = apiURL.isEmpty() ? apiURL : apiURL + ORGS_URI;
        JSONArray orgsJSON = getJsonArray(orgsURL, token);
        Set<String> orgIds = new HashSet<String>();
        log.trace("orgsJSON " + orgsJSON);
        createOrgs(companyId, orgsJSON, false, orgIds);
        String deptsURL = apiURL.isEmpty() ? apiURL : apiURL + DEPTS_URI;
        JSONArray deptsJSON = getJsonArray(deptsURL, token);
        log.trace("deptsJSON " + deptsJSON);
        createOrgs(companyId, deptsJSON, true, orgIds);
        if (PrefsPropsUtil.getBoolean(companyId,"IMPORT_DELETE_ENABLED", true)) {
            deleteOrganizations(companyId, orgIds);
        }
    }

    private static void deleteOrganizations(long companyId, Set<String> orgIds) {
        try {
            List<ExpandoValue> expandoValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId,
                    Organization.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME, WebKeys.OAUTH_ID,
                    QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            for (ExpandoValue it: expandoValues) {
                if (!it.getString().isEmpty() && orgIds.contains(it.getString())) {
                    try {
                        OrganizationLocalServiceUtil.deleteOrganization(it.getClassPK());
                        log.info("delete organization with id " + it.getString());
                    } catch (Exception e) {
                        log.error("error delete organization: " + e.getClass().getName() + " " + GetterUtil.getString(e.getMessage()));
                        log.debug(e);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void createOrgs(long companyId, JSONArray orgsJSON, boolean isDept, Set<String> orgIds) throws SystemException {
        try {
            long defaultUserId = UserLocalServiceUtil.getDefaultUserId(companyId);
            String companyScope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE, StringPool.BLANK);
            if (!companyScope.isEmpty()) {
                for (int i = 0; i < orgsJSON.length(); i++) {
                    try {
                        JSONObject orgJSON = orgsJSON.getJSONObject(i);
                        String status = orgJSON.getString("status");
                        String id = orgJSON.getString("id");
                        String orgName = orgJSON.getString(isDept ? "name" : "orgName");
                        String parentId = StringPool.BLANK;
                        String scope = orgJSON.getString("userDomain").toLowerCase();
                        if (scope.equals(companyScope)) {
                            if (isDept) {
                                String parId = GetterUtil.getString(orgJSON.getString("parentId"), StringPool.BLANK);
                                if (!parId.isEmpty() || !parId.equals("null")) {
                                    parentId = parId;
                                } else {
                                    String comId = GetterUtil.getString(orgJSON.getString("companyId"), StringPool.BLANK);
                                    if (!comId.isEmpty() || !comId.equals("null")) {
                                        parentId = comId;
                                    }
                                }
                            }
                            // Need for duplicate names
                            /*if (isDept && !parentId.isEmpty()) {
                                orgName = orgName + "(" + parentId + ")";
                            }*/
                            if (id != null && !id.isEmpty()) {
                                Organization org = getOrgByExpando(companyId, WebKeys.OAUTH_ID, id);
                                if (Validator.isNotNull(org)) {
                                    if (status.equals(STATUS_A)) {
                                        org.setName(orgName);
                                        if (isDept) {
                                            Organization parentOrg = getOrgByExpando(companyId, WebKeys.OAUTH_ID, parentId);
                                            if (Validator.isNotNull(parentOrg)) {
                                                org.setParentOrganizationId(parentOrg.getOrganizationId());
                                            }
                                        }
                                        try {
                                            OrganizationLocalServiceUtil.updateOrganization(org);
                                            log.info("update organization with id " + id + " : " + orgJSON.toString());
                                        } catch (Exception e) {
                                            log.error("Can't update organization " + orgName
                                                    + " with organizationId " + org.getOrganizationId()
                                                    + " and id " + id + StringPool.NEW_LINE + e.getMessage());
                                        }
                                    }
                                } else if (status.equals(STATUS_A)) {
                                    long parenOrgId = OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID;
                                    if (isDept) {
                                        Organization parentOrg = getOrgByExpando(companyId, WebKeys.OAUTH_ID, parentId);
                                        if (Validator.isNotNull(parentOrg)) {
                                            parenOrgId = parentOrg.getOrganizationId();
                                        }
                                    }
                                    try {
                                        org = OrganizationLocalServiceUtil.addOrganization(defaultUserId, parenOrgId, orgName, false);
                                        log.info("update organization with id " + id + " : " + orgJSON.toString());
                                    } catch (DuplicateOrganizationException e) {
                                        log.error(e.getMessage());
                                    }
                                }
                                if (Validator.isNotNull(org)) {
                                    ExpandoBridge expandoBridge = org.getExpandoBridge();
                                    if (!expandoBridge.hasAttribute(WebKeys.OAUTH_ID)) {
                                        expandoBridge.addAttribute(WebKeys.OAUTH_ID, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
                                    }
                                    expandoBridge.setAttribute(WebKeys.OAUTH_ID, id, false);
                                    if (status.equals(STATUS_D)) {
                                        orgIds.add(id);
                                    }
                                }
                            } else {
                                throw new SystemException("id is empty or null");
                            }
                        }
                    } catch (SystemException | PortalException e) {
                        log.error(e);
                    }
                }
            } else {
                log.warn("Can't import organizations for companyId " + companyId + ", oauth scope is empty!");
            }
        } catch (PortalException pe) {
            throw new SystemException(pe);
        }
    }

    private static Organization getOrgByExpando(long companyId, String key, String value) throws SystemException {
        Organization organization = null;
        List<ExpandoValue> exValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId,
                Organization.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
                key, value, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
        if (exValues != null && exValues.size() > 0) {
            ExpandoValue exValue = exValues.get(0);
            long orgaganizationId = exValue.getClassPK();
            organization = OrganizationLocalServiceUtil.fetchOrganization(orgaganizationId);
        }
        return organization;
    }

    private static JSONArray getJsonArray(String url, String token) throws SystemException {
        JSONArray array = null;
        Header bearer = new BasicHeader("Authorization", "Bearer " + token);
        Header[] headers = new Header[]{bearer};
        String strResult = RequestUtil.sendGet(url, headers);
        try {
            array = JSONFactoryUtil.createJSONArray(strResult);
        } catch (JSONException e) {
            throw new SystemException(e);
        }
        return array;
    }

    private static String getAppToken(long companyId) throws SystemException {
        String token = null;
        try {
            String url = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_TOKEN_URL, StringPool.BLANK);
            if (!url.isEmpty()) {
                List<NameValuePair> urlParameters = new ArrayList<>();
                urlParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));
                urlParameters.add(new BasicNameValuePair("scope", "sync"));
                urlParameters.add(new BasicNameValuePair("client_id", PrefsPropsUtil.getString(companyId,
                        PortletKeys.OAUTH_CLIENT_ID_IMPORT, StringPool.BLANK)));
                urlParameters.add(new BasicNameValuePair("client_secret", PrefsPropsUtil.getString(companyId,
                        PortletKeys.OAUTH_CLIENT_SECRET_IMPORT, StringPool.BLANK)));
                String jsonStr = RequestUtil.sendPost(url, urlParameters, null);
                JSONObject jsonObject = JSONFactoryUtil.createJSONObject(jsonStr);
                if (jsonObject.has("error")) {
                    throw new SystemException(jsonObject.getString("error_description"));
                } else if (!jsonObject.has("access_token")) {
                    throw new SystemException("response not contains access_token");
                } else {
                    token = jsonObject.getString("access_token");
                }
            }
        } catch (JSONException e) {
            throw new SystemException(e);
        }
        return token;
    }

    public static void startImportUsers() {
        List<Company> companies = null;
        try {
            companies = CompanyLocalServiceUtil.getCompanies();
        } catch (Exception ex) {
            log.error(ex);
        }
        if (companies != null && !companies.isEmpty()) {
            for (Company company: companies) {
                try {
                    if (GetterUtil.getBoolean(PrefsPropsUtil.getBoolean(company.getCompanyId(),
                            PortletKeys.OAUTH_ENABLED, false))) {
                        getUsers(company.getCompanyId());
                    }
                } catch (SystemException se) {
                    log.error("Can't import organizations for " + company.getCompanyId(), se);
                }
            }
        }
    }

    private static void getUsers(long companyId) throws SystemException {
        String apiURL = PrefsPropsUtil.getString(companyId, PortletKeys.API_URL, StringPool.BLANK);
        String usersURL = apiURL.isEmpty() ? apiURL : apiURL + USERS_URI;
        if (!usersURL.isEmpty()) {
            String token = getAppToken(companyId);
            JSONArray usrsJSON = getJsonArray(usersURL, token);
            log.trace("usrsJSON: " + usrsJSON.toString());
            Set<String> userIds = new HashSet<String>();
            createOrUpdUsers(companyId, usrsJSON, userIds);
            if (PrefsPropsUtil.getBoolean(companyId,"IMPORT_DELETE_ENABLED", true)) {
                deleteUsers(companyId, userIds);
            }
        }
    }

    private static void deleteUsers(long companyId, Set<String> userIds) {
        try {
            List<ExpandoValue> expandoValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId, User.class.getName(),
                    ExpandoTableConstants.DEFAULT_TABLE_NAME, WebKeys.OAUTH_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            for (ExpandoValue value : expandoValues) {
                if (!value.getString().isEmpty() && userIds.contains(value.getString())) {
                    try {
                        UserLocalServiceUtil.deleteUser(value.getClassPK());
                        log.info("delete user with id " + value.getString());
                    } catch (Exception e) {
                        log.error("error delete user: " + e.getClass().getName() + " " + GetterUtil.getString(e.getMessage()));
                        log.debug(e);
                    }
                    ExpandoValueLocalServiceUtil.deleteValue(value);
                }
            }
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void createOrUpdUsers(long companyId, JSONArray usersJSON, Set<String> userIds) throws SystemException {
        try {
            long defaultUserId = UserLocalServiceUtil.getDefaultUserId(companyId);
            String companyScope = PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE, StringPool.BLANK);
            if (!companyScope.isEmpty()) {
                for (int i = 0; i < usersJSON.length(); i++) {
                    try {
                        JSONObject userJSON = usersJSON.getJSONObject(i);
                        String scope = getJsonKey(userJSON, "userDomain").toLowerCase();
                        if (scope.equals(companyScope)) {
                            String id = getJsonKey(userJSON, "id");
                            String status = getJsonKey(userJSON, "status");
                            String email = getJsonKey(userJSON, "email");
                            String firstName = getJsonKey(userJSON, "firstName");
                            String secondName = getJsonKey(userJSON, "secondName");
                            String lastName = getJsonKey(userJSON, "lastName");
                            String birthDate = getJsonKey(userJSON, "birthDate");
                            String deptId = getJsonKey(userJSON, "deptId");
                            String deptName = getJsonKey(userJSON, "deptName");
                            String orgId = getJsonKey(userJSON, "orgId");
                            String workPosition = getJsonKey(userJSON, "workPosition");
                            String organizationId = (Validator.isNotNull(deptId)) ? deptId : orgId;
                            /*if (email.isEmpty()) {
                                Company company = CompanyLocalServiceUtil.fetchCompany(companyId);
                                email = FriendlyURLNormalizerUtil.normalize(lastName.toLowerCase()) + StringPool.AT + company.getMx();
                            }*/
                            if (id != null && !id.isEmpty()) {
                                User user = getUserByExp(companyId, WebKeys.OAUTH_ID, id);
                                if (Validator.isNotNull(user)) {
                                    if (status.equals(STATUS_A)) {
                                        user.setEmailAddress(email);
                                        user.setFirstName(firstName);
                                        user.setLastName(lastName);
                                        user.setMiddleName(secondName);
                                        user.setJobTitle(workPosition);
                                        user = UserLocalServiceUtil.updateUser(user);
                                        if (Validator.isNotNull(birthDate)) {
                                            Date date = null;
                                            try {
                                                date = sdf.parse(birthDate);
                                            } catch (ParseException e) {
                                                log.error(e.getMessage());
                                            }
                                            Contact contact = user.getContact();
                                            contact.setBirthday(date);
                                            ContactLocalServiceUtil.updateContact(contact);
                                        }
                                        if (Validator.isNotNull(organizationId)) {
                                            Organization organization = getOrgByExpando(companyId, WebKeys.OAUTH_ID, organizationId);
                                            if (Validator.isNotNull(organization)) {
                                                UserLocalServiceUtil.addOrganizationUser(organization.getOrganizationId(), user);
                                            }
                                        }
                                        log.info("update user with id " + id  + " : " + userJSON.toString());
                                    }
                                } else if (status.equals(STATUS_A)) {
                                    long[] organizationIds = null;
                                    if (Validator.isNotNull(organizationId)) {
                                        Organization organization = getOrgByExpando(companyId, WebKeys.OAUTH_ID, organizationId);
                                        if (Validator.isNotNull(organization)) {
                                            organizationIds = new long[]{organization.getOrganizationId()};
                                        }
                                    }
                                    Calendar birth = Calendar.getInstance();
                                    birth.set(Calendar.YEAR, 1970);
                                    birth.set(Calendar.MONTH, Calendar.JANUARY);
                                    birth.set(Calendar.DAY_OF_MONTH, 1);
                                    if (Validator.isNotNull(birthDate)) {
                                        Date date = null;
                                        try {
                                            date = sdf.parse(birthDate);
                                        } catch (ParseException e) {
                                            log.error(e.getMessage());
                                        }
                                        if (Validator.isNotNull(date)) {
                                            birth.setTime(date);
                                        }
                                    }
                                    try {
                                        user = UserLocalServiceUtil.addUser(defaultUserId, companyId, true,
                                                StringPool.BLANK, StringPool.BLANK, true, StringPool.BLANK,
                                                email, 0l, StringPool.BLANK, null,
                                                firstName, secondName,
                                                lastName, 0, 0, true,
                                                birth.get(Calendar.MONTH), birth.get(Calendar.DAY_OF_MONTH), birth.get(Calendar.YEAR), workPosition,
                                                null, organizationIds, null, null, false, new ServiceContext());
                                        log.info("add user with id " + id  + " : " + userJSON.toString());
                                    } catch (DuplicateUserEmailAddressException e) {
                                        log.error("Can't add user with email " + email + ", user already exist! id = " + id);
                                    } catch (UserEmailAddressException ue) {
                                        log.error("Can't add user with email " + email + "! id = " + id);
                                    }
                                }
                                if (Validator.isNotNull(user)) {
                                    ExpandoBridge expandoBridge = user.getExpandoBridge();
                                    if (!expandoBridge.hasAttribute(WebKeys.OAUTH_ID)) {
                                        expandoBridge.addAttribute(WebKeys.OAUTH_ID, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
                                    }
                                    expandoBridge.setAttribute(WebKeys.OAUTH_ID, id);
                                    if (status.equals(STATUS_D)) {
                                        userIds.add(id);
                                    }
                                    if (!expandoBridge.hasAttribute(WebKeys.DEPT_NAME)) {
                                        expandoBridge.addAttribute(WebKeys.DEPT_NAME, ExpandoColumnConstants.STRING, StringPool.BLANK, false);
                                    }
                                    expandoBridge.setAttribute(WebKeys.DEPT_NAME, deptName);
                                }
                            } else {
                                throw new SystemException("id is empty or null");
                            }
                        }
                    } catch(Exception e) {
                        log.error(e);
                    }
                }
            } else {
                log.warn("Can't import users for companyId " + companyId + ", oauth scope is empty!");
            }
        } catch (PortalException e) {
            throw new SystemException(e);
        }
    }

    public static String getJsonKey(JSONObject jsonObject, String key) {
        String value = null;
        if (jsonObject.has(key)) {
            if (!jsonObject.getString(key).equals("null")) {
                value = jsonObject.getString(key);
            }
        }
        return value;
    }

    public static User getUserByExp(long companyId, String key, String value) throws SystemException {
        User user = null;
        List<ExpandoValue> exValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId,
                User.class.getName(), ExpandoTableConstants.DEFAULT_TABLE_NAME,
                key, value, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
        if (exValues != null && exValues.size() > 0) {
            ExpandoValue exValue = exValues.get(0);
            long classPK = exValue.getClassPK();
            user = UserLocalServiceUtil.fetchUser(classPK);
        }
        return user;
    }
}
