package ru.emdev.minudo.util;

public class PortletKeys {
    public static final String OAUTH_ENABLED = "OAUTH_ENABLED";
    public static final String OAUTH_SCOPE = "OAUTH_SCOPE";
    public static final String OAUTH_CLIENT_ID = "OAUTH_CLIENT_ID";
    public static final String OAUTH_CLIENT_ID_IMPORT = "OAUTH_CLIENT_ID_IMPORT";
    public static final String OAUTH_CLIENT_SECRET = "OAUTH_CLIENT_SECRET";
    public static final String OAUTH_CLIENT_SECRET_IMPORT = "OAUTH_CLIENT_SECRET_IMPORT";
    public static final String OAUTH_URL = "OAUTH_URL";
    //public static final String OAUTH_REDIRECT_URL = "OAUTH_REDIRECT_URL";
    public static final String OAUTH_TOKEN_URL = "OAUTH_TOKEN_URL";
    public static final String OAUTH_TOKEN_CHECK_URL = "OAUTH_TOKEN_CHECK_URL";
    public static final String RESOURCE_SERVER_KEY = "RESOURCE_SERVER_KEY";
    public static final String RESOURCE_SERVER_PASSWORD = "RESOURCE_SERVER_PASSWORD";
    /*public static final String ORGS_URL = "ORGS_URL";
    public static final String DEPTS_URL = "DEPTS_URL";
    public static final String USERS_URL = "USERS_URL";*/
    public static final String API_URL = "API_URL";
    public static final String OAUTH_LOGOUT_URL = "OAUTH_LOGOUT_URL";
}
