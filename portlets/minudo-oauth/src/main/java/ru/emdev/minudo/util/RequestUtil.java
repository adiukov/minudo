package ru.emdev.minudo.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RequestUtil {

    private static final Log log = LogFactoryUtil.getLog(RequestUtil.class);

    public static String sendPost(String url, List<NameValuePair> urlParameters, Header[] headers) {
        String resultStr = null;
        try {
            HttpPost httpReq = new HttpPost(url);
            if (headers != null) {
                httpReq.setHeaders(headers);
            }
            httpReq.setEntity(new UrlEncodedFormEntity(urlParameters));
            CloseableHttpResponse response = (CloseableHttpResponse) HttpClientBuilder.
                    create().build().execute(httpReq);
            resultStr = readFromResponse(response);
        } catch (Exception e) {
            log.error(e);
        }
        return resultStr;
    }

    public static String sendGet(String url, Header[] headers) {
        String resultStr = null;
        try {
            HttpGet httpReq = new HttpGet(url);
            if (headers != null) {
                httpReq.setHeaders(headers);
            }
            CloseableHttpResponse response = (CloseableHttpResponse) HttpClientBuilder.
                    create().build().execute(httpReq);
            resultStr = readFromResponse(response);
        } catch (Exception e) {
            log.error(e);
        }
        return resultStr;
    }

    private static String readFromResponse(CloseableHttpResponse response) throws IOException {
        String resultStr = StringPool.BLANK;
        BufferedReader bufferedReader = null;
        StringBuffer result = null;
        try{
            bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()));
            result = new StringBuffer();
            String line = StringPool.BLANK;
            while ((line = bufferedReader.readLine()) != null){
                result.append(line);
            }
            resultStr = result.toString();
        } finally{
            bufferedReader.close();
            response.close();
        }
        return resultStr;
    }
}
