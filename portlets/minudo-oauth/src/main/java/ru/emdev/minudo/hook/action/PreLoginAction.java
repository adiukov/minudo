package ru.emdev.minudo.hook.action;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import ru.emdev.minudo.util.PortletKeys;
import ru.emdev.minudo.util.WebKeys;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class PreLoginAction extends Action {

    private static Log log = LogFactoryUtil.getLog(PreLoginAction.class);

    private static String USE_ADMIN_LOGIN = "use_admin_login";

    @Override
    public void run(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ActionException {
        ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
        HttpServletRequest request = PortalUtil.getOriginalServletRequest(httpServletRequest);
        HttpSession session = httpServletRequest.getSession();
        if (!themeDisplay.isSignedIn()) {
            String uri = request.getRequestURI();
            if (uri.startsWith("/c/")) {
                uri = uri.replace("/c/", "/");
            }
            String authPublicPaths = PropsUtil.get(PropsKeys.AUTH_PUBLIC_PATHS);
            log.debug("uri " + uri);
            String url = themeDisplay.getURLCurrent();
            log.debug("url " + url);
            log.debug("http url " + request.getRequestURL());
            if (url.contains("/group/control_panel") || url.contains("/c/portal/login?redirect=%2Fgroup%2Fcontrol_panel")) {
                session.setAttribute("use_admin_login", Boolean.TRUE);
            }
            boolean useAdminLogin = GetterUtil.getBoolean(session.getAttribute(USE_ADMIN_LOGIN), false);
            log.debug("useAdminLogin1 " + useAdminLogin);
            if (!authPublicPaths.contains(uri)
                    && !useAdminLogin) {
                log.debug("in redirect");
                long companyId = themeDisplay.getCompanyId();
                try {
                    boolean oauthEnabled = PrefsPropsUtil.getBoolean(companyId, PortletKeys.OAUTH_ENABLED, Boolean.FALSE);
                    if (oauthEnabled) {
                        String redirectURL = themeDisplay.getURLCurrent();
                        redirectURL = StringUtil.replace(redirectURL, "//", "/");
                        StringBuilder sb = new StringBuilder();
                        sb.append(PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_URL));
                        sb.append("?response_type=code");
                        sb.append("&client_id=" + PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_CLIENT_ID));
                        sb.append("&redirect_uri=" + themeDisplay.getURLPortal() + "/c/login/oauth");
                        sb.append("&scope=" + PrefsPropsUtil.getString(companyId, PortletKeys.OAUTH_SCOPE));
                        sb.append("&state=" + redirectURL);
                        httpServletResponse.sendRedirect(sb.toString());
                    }
                } catch (SystemException | IOException e) {
                    log.error(e);
                }
            }
        } else {
            session.setAttribute(USE_ADMIN_LOGIN, Boolean.FALSE);
        }
    }
}
